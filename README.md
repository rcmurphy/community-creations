# Alliance Auth Community Creations

A directory of community sourced plugins, tools, resources, etc. for Alliance Auth

## Contents

- [Plugins](#plugins)
- [Services](#services)
- [Deployment and Administration](#deployment-and-administration)
- [Developer resources](#developer-resources)

## Plugins

- [Alliance Auth / allianceauth-bbs](https://gitlab.com/allianceauth/allianceauth-bbs) - A simple forum for Allianceauth
- [Basraah / allianceauth-idp](https://gitlab.com/basraah/allianceauth-idp) - SAML 2.0 Identity Provider for Alliance Auth
- [Basraah / standingsrequests](https://gitlab.com/basraah/standingsrequests) - Alliance Auth compatible standings tool module for requesting alt character standings and checking API key registration.
- [colcrunch / fittings](https://gitlab.com/colcrunch/fittings) - A plugin for managing fittings and doctrines.
- [colcrunch / aa-moonstuff](https://gitlab.com/colcrunch/aa-moonstuff) - A plugin for publishing moon extractions, and keeping track of moon scan data.
- [Erik Kalkoken / aa-freight](https://gitlab.com/ErikKalkoken/aa-freight.git) - A plugin for running a central freight service for an alliance.
- [Erik Kalkoken / aa-dashboard-alerts](https://gitlab.com/ErikKalkoken/aa-dashboard-alerts) - Adds an alert message to the dashboard to notify users about their current open items like pending group requests and unread notifications.
- [Erik Kalkoken / aa-killtracker](https://gitlab.com/ErikKalkoken/aa-killtracker) - An app for running killmail trackers with Alliance Auth and Discord
- [Erik Kalkoken / aa-memberaudit](https://gitlab.com/ErikKalkoken/aa-memberaudit) - An app that provides full access to Eve characters and related reports for auditing, vetting and monitoring.
  - [Eclipse Expeditions / aa-memberaudit-securegroups](https://gitlab.com/eclipse-expeditions/aa-memberaudit-securegroups) - Integration with [Secure Groups](https://github.com/pvyParts/allianceauth-secure-groups) for automatic group management based on activity, character age, assets, compliance, SP, or skill sets.
- [Erik Kalkoken / aa-package-monitor](https://gitlab.com/ErikKalkoken/aa-package-monitor) - An app that helps keep track of installed packages and outstanding updates for Alliance Auth.
- [Erik Kalkoken / aa-standingssync](https://gitlab.com/ErikKalkoken/aa-standingssync.git) - Enables non-alliance characters like scout alts to have the same standings view in game as their alliance main
- [Erik Kalkoken / aa-structuretimers](https://gitlab.com/ErikKalkoken/aa-structuretimers) - An app for keeping track of Eve Online structure timers with Alliance Auth and Discord
- [Erik Kalkoken / aa-structures](https://gitlab.com/ErikKalkoken/aa-structures) - A plugin for monitoring alliance structures incl. a structure browser and structure notifications on Discord.
- [AaronKable / aa-corpstats-two](https://github.com/pvyParts/allianceauth-corpstats-two) - Extended Corpstats module for AllianceAuth with speed and functionality in mind.
- [AaronKable / allianceauth-signal-pings](https://github.com/pvyParts/allianceauth-signal-pings) - A simple plugin to send a "signal" to a discord webhook when a user does something in auth.
- [AaronKable / allianceauth-group-assigner](https://github.com/pvyParts/allianceauth-group-assigner) - Admin app to add groups to users when they change States.
- [AaronKable / aa-discordbot](https://github.com/pvyParts/allianceauth-discordbot) - Discordbot for AllianceAuth that can interact with the Django project, as well as send Channel and Direct Messages.
- [ppfeufer / aa-esi-status](https://github.com/ppfeufer/aa-esi-status) - App for Alliance Auth to show the current status of ESI and its end points.
- [ppfeufer / aa-fleetpings](https://github.com/ppfeufer/aa-fleetpings) - App that can format your fleet pings and also ping for you to Discord and Slack.
- [ppfeufer / aa-sov-timer](https://github.com/ppfeufer/aa-sov-timer) - Sovereignty campaign overview for Alliance Auth.
- [ppfeufer / aa-timezones](https://github.com/ppfeufer/aa-timezones) - Displaying different time zones within Alliance Auth.
- [mckernanin / aa-graphql](https://github.com/mckernanin/aa-graphql) - GraphQL API For Alliance Auth
- [Evictus / ImicusFAT](https://gitlab.com/evictus.iou/allianceauth-imicusfat) - An Improved FAT/PAP System for Alliance Auth.
- [Ariel Rin / aa-gdpr](https://gitlab.com/soratidus999/aa-gdpr) - A Collection of resources to help Alliance Auth installs meet GDPR legislations.
- [Ariel Rin / aa-relays](https://gitlab.com/soratidus999/aa-relays) - For forwarding, collating and filtering of messages from various chat services to defined outputs including Database logging
- [Eclipse Expeditions / aa-blueprints](https://gitlab.com/eclipse-expeditions/aa-blueprints) - An app that provides a list of corporate blueprints for reference by members.
- [Eclipse Expeditions / aa-inactivity](https://gitlab.com/eclipse-expeditions/aa-inactivity) - An app that allows corps to track member activity as well as leave of absence requests.
- [pksunakra / aa-buybacks](https://gitlab.com/pksunkara/aa-buybacks) - An app that allows corps to manage their buyback programs and also allows their members to use the above programs.

## Services

- [Alliance Auth / mumble-authenticator](https://gitlab.com/allianceauth/mumble-authenticator) - Authenticator script for the Alliance Auth Mumble integration
- [AaronKable / allianceauth-mumbletemps](https://github.com/pvyParts/allianceauth-mumble-temp) - Enables Templink access to Mumble for non authed characters
- [AaronKable / allianceauth-wiki-js](https://github.com/pvyParts/allianceauth-wiki-js) - Allianceauth service module for Wiki.js

## Deployment and Administration

- [AaronKable / AllianceAuth-Celery-Analytics](https://github.com/pvyParts/allianceauth-celeryanalytics) - Celery task output logging to database for easy viewing and monitoring
- [MillerUK / AllianceAuth-Docker](https://github.com/milleruk/alliance_auth_docker) -  Docker-Compose Stack - Including Traefik Proxy
- [MillerUK/ AllianceAuth-Docker-Image](https://hub.docker.com/r/milleruk/allianceauth) - Docker image for AllianceAuth
- [mckernanin / Alliance Auth Kubernetes](https://github.com/mckernanin/alliance-auth-kubernetes) - Tutorial on setting up AA on a kubernetes cluster

## Developer resources

- [Erik Kalkoken / allianceauth-example-plugin](https://gitlab.com/ErikKalkoken/allianceauth-example-plugin) - Example plugin app that can be used as starting point / template for developing new plugin apps
- [Erik Kalkoken / django-eveuniverse](https://gitlab.com/ErikKalkoken/django-eveuniverse) - Complete set of Eve Online Universe models for Django with on-demand loading from ESI
- [Erik Kalkoken / a-dev-setup-wsl-vsc-v2.md](https://gist.github.com/ErikKalkoken/6fbcc8f27a0840836a811760d8c47216) - Guide on how to setup an Alliance Auth development environment on Windows 10 with WSL and Visual Studio Code
